import "./CountryCard.css";
import { Link,useNavigate } from "react-router-dom";

export const CountryCard = (props) => {
  let data = props.data;

  return (
    <>
      <Link to={`/country/${data.cca3}`}>
        {<div className="country-card-container">
          <div className="country">
            <div className="flag-container">
              <img
                className="country-flag"
                src={data.flags.png}
                alt={data.flags.alt}
              ></img>
            </div>

            <div className="country-information">
              <div className="name">{data.name.common}</div>
              <div className="population">population: {data.population}</div>
              <div className="region">Region: {data.region}</div>
              <div className="capital">capital: {data.capital}</div>
            </div>
          </div>
        </div>}
        </Link>
      
    </>
  );
};
