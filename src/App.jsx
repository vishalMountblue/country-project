import "./App.css";

import { Route, Routes } from "react-router-dom";
import { Home } from "./Home";
import { CountryPage } from "./CountryPage";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/country/:id" element={<CountryPage/>}></Route>
      </Routes>
    </>
  );
}

export default App;
