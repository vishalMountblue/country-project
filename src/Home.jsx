import axios from "axios";
import { useState, useEffect } from "react";
import { CountryCard } from "./CountryCard";
import { Header } from "./Header";
import "./Home.css";

export const Home = () => {
  const [countryData, setCountryData] = useState([]);
  const [region, setRegion] = useState("all");
  const [searchText, setSearchText] = useState("");
  const [apiError, setApiError] = useState(false);
  let continentArray = [
    "all",
    "Asia",
    "Europe",
    "Oceania",
    "Africa",
    "Antarctica",
    "North America",
    "South America",
  ];

  const handleFilterByRegion = (event) => {
    setRegion(event.target.value);
  };

  const handleSearch = (event) => {
    setSearchText(event.target.value);
  };

  let filteredCountries = null;

  if (searchText !== "" && region === "all") {
    filteredCountries = countryData.filter((country) => {
      if (
        country.name.common
          .toLowerCase()
          .startsWith(searchText.toLocaleLowerCase())
      )
        return country;
    });
  } else {
    filteredCountries =
      region === "all" && searchText === ""
        ? countryData
        : countryData.filter((country) => {
            if (
              country.region === region &&
              country.name.common
                .toLowerCase()
                .startsWith(searchText.toLocaleLowerCase())
            )
              return country;
          });
  }

  useEffect(() => {
    const fetchAllCountriesData = async () => {
      try {
        let response = await axios.get("https://restcountries.com/v3.1/all");
        setCountryData(response.data);
        setApiError(false);
      } catch (error) {
        setApiError(true);
        
        console.error("error", error);
      }
    };
    fetchAllCountriesData();
  }, []);

  return (
    <>
      <Header />

      <div className="main-container-wrapper">
        {countryData.length > 0 ? (
          <main>
            <div className="search-filter-container">
              <div className="search-container">
                <div className="search-icon-container">
                  <img className="icon" src="/search.png"></img>
                </div>

                <input
                  onChange={handleSearch}
                  type="search"
                  placeholder="Search for a country..."
                  className="search-text"
                ></input>
              </div>
              <div className="filter-container">
                <select
                  onChange={handleFilterByRegion}
                  name="continent"
                  id="continent"
                >
                  {continentArray.map((continent) => {
                    return <option value={continent}>{continent}</option>;
                  })}
                </select>
              </div>
            </div>

            {filteredCountries.length > 0 ? (
              <div className="country-card-wrapper">
                {filteredCountries.map((country) => {
                  
                  return <CountryCard key={country.name.common} data={country} />;
                })}
              </div>
            ) : (
              <div className="default-message">No Country Found!</div>
            )}
          </main>
        ) : apiError ? (
          <div className="default-message">
            Error In Fetching Countries!
          </div>
        ) : (
          <div className="default-message">Loading...</div>
        )}
      </div>
    </>
  );
};
