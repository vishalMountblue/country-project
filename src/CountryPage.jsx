import { Header } from "./Header";
import axios from "axios";
import { useParams, Link } from "react-router-dom";
import { useState, useEffect } from "react";
import "./CountryPage.css";
export const CountryPage = () => {
  const { id } = useParams();

  const [borderId, setBorderId] = useState(id);

  let [countryData, setCountryData] = useState([]);

  useEffect(() => {
    const fetchSingleCountryData = async () => {
      try {
        const response = await axios.get(
          `https://restcountries.com/v3.1/alpha/${borderId}`
        );
        setCountryData(response.data);
      } catch (error) {
        console.error("error", error);
      }
    };

    fetchSingleCountryData();
  }, [borderId]);

  let data = countryData[0];
  let borderCountries = [];
  let languages = {};
  let nativeName = null;
  let currencies = null;

  if (data) {
    borderCountries = data.borders;

    if (data.language) {
      languages = Object.values(data.languages);
    }

    if (data.name.nativeName) {
      nativeName = Object.values(data.name.nativeName)[0].official;
    }

    if (data.currencies) {
      currencies = Object.values(data.currencies)[0].name;
    }
  }

  return (
    <>
      <Header />

      <main>
        <Link to="/">
          <button className="back-button">
            <img
              className="icon"
              src="/left-arrow.png"
              alt="left arrow icon"
            ></img>
            Back
          </button>
        </Link>

        <section>
          <div className="country-information-container">
            <div className="country-flag-container">
              <img className="large-image" src={data?.flags.png}></img>
            </div>
            <div className="country-description-wrapper">
              <div className="name">
                {data?.name.common ? data.name.common : "N/A"}
              </div>

              <div className="country-description-container">
                <div className="basic-description">
                  <div className="native-name">
                    native name: {nativeName ? nativeName : "N/A"}
                  </div>
                  <div className="population">
                    population: {data?.population}
                  </div>
                  <div className="region">
                    region: {data?.region ? data?.region : "N/A"}
                  </div>
                  <div className="sub-region">
                    sub-region: {data?.subregion ? data?.subregion : "N/A"}
                  </div>
                  <div className="capital">
                    capital: {data?.capital ? data?.capital : "N/A"}
                  </div>
                </div>
                <div className="other-description">
                  <div className="top-level-domain">
                    top-level-domain: {data?.tld[0]}
                  </div>
                  <div className="currencies">
                    currencies {currencies ? currencies : "N/A"}
                  </div>
                  <div className="language">
                    language:
                    {languages.length > 0
                      ? languages.map((language) => {
                          return language + ",";
                        })
                      : "N/A"}
                  </div>
                </div>
              </div>

              <div className="border-countries-container">
                <div className="border-text">Border Countries: </div>

                {borderCountries
                  ? borderCountries.map((country) => {
                      return (
                        <Link to={`/country/${country}`}>
                          <div
                            onClick={() => setBorderId(country)}
                            className="border-box"
                          >
                            {country}
                          </div>
                        </Link>
                      );
                    })
                  : "N/A"}
              </div>
            </div>
          </div>
        </section>
      </main>
    </>
  );
};
