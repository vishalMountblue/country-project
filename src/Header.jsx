import "./Header.css";
export const Header = () => {
  return (
    <>
      <header>
        <div className="heading">Where in the world?</div>
        <div className="mode">
          <img className="icon" src="/moon.png"></img>
          <div className="mode-text">Dark Mode</div>
        </div>
      </header>
    </>
  );
};
